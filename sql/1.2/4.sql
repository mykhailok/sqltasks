alter table projects add cost numeric(10,2);
update projects set cost = 1000000 where project_name = 'Financial Software';

update projects set cost = 1000000 where project_name = 'Ticketing Software';
update projects set cost = 1000000 where project_name = 'Website Project';

update projects set cost = 1000000 where project_name = 'CMS Software';
update projects set cost = 1000000 where project_name = 'Website Architecture';