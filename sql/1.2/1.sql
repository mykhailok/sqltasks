alter table developers add salary numeric(10,2);

update developers set salary = 50000 where last_name = 'Kosinskyi';
update developers set salary = 20000 where last_name = 'Len''';

update developers set salary = 30000 where last_name = 'Malikov';
update developers set salary = 40000 where last_name = 'Senchuk';

update developers set salary = 10000 where last_name = 'Volkov';