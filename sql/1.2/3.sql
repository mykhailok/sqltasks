SELECT sum(developers.salary), skills.skill_name from skills, developers, developers_skills
WHERE developers.id = developers_skills.developer_id AND skills.id = developers_skills.skill_id AND skills.skill_name LIKE 'Java'
GROUP BY skills.skill_name;