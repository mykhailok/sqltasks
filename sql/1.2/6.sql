SELECT projects.id, (projects.cost - sum(developers.salary)) cost_minus_salary
  , avg(developers.salary) avg_salary, projects.project_name
FROM developers, projects_developers, projects
WHERE developers.id = projects_developers.developer_id AND projects.id = projects_developers.project_id
GROUP BY projects.id
ORDER BY cost_minus_salary
LIMIT 1;
