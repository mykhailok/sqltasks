SELECT company_id, company_name, MIN(vytorg_pokupatelya) AS minimalnyj_vytorg, customer_name
FROM
  (
    SELECT companies.id  AS company_id, companies.company_name AS company_name
      , SUM(vytorg_proekta) AS vytorg_pokupatelya, customers.customer_name AS customer_name
    FROM
      (
        SELECT projects.id AS id, (projects.cost - SUM(salary)) AS vytorg_proekta
        FROM projects
          JOIN projects_developers
            ON projects.id = project_id
          JOIN developers
            ON developers.id = developer_id
        GROUP BY projects.id
      ) AS vytorg_proektov

      JOIN projects
        ON vytorg_proektov.id = projects.id
      JOIN companies
        ON company_id = companies.id
      JOIN customers
        ON customer_id = customers.id
    GROUP BY companies.id, customers.id
  ) AS vytorg_kompanii_klienta
GROUP BY  company_id, company_name, customer_name
ORDER BY company_id;